# PICC Reader Broker

[![Build Status](https://travis-ci.com/openfarecollection/oafc-picc-reader-broker.svg?branch=develop)](https://travis-ci.com/bitbucket/openfarecollection/oafc-picc-reader-broker)


### Build and run
```bash
./gradlew clean build
java -jar build/libs/picc-reader-broker.jar
```

### Generate test metrics
```bash
./gradlew clean pitest
```

### Sonar
```bash
./gradlew clean build sonarqube -Dsonar.host.url=http://localhost:9000
```

### Local deployment
```bash

# Set Docker environment
eval $(minikube docker-env)

# Create config map
kubectl create configmap picc-reader-broker-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml

# Build
./gradlew clean build

# Docker build
docker build -t local/picc-reader-broker:$(git rev-parse --short HEAD) .

# Helm install
helm upgrade --install --force picc-reader-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/picc-reader-broker:$(git rev-parse --short HEAD)

# All together
./gradlew clean build && \
docker build -t local/picc-reader-broker:$(git rev-parse --short HEAD) . && \
helm upgrade --install --force picc-reader-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/picc-reader-broker:$(git rev-parse --short HEAD)


```