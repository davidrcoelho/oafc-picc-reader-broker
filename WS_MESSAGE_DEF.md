# PICC Reader Broker - Websocket Messages

<small>to connect</small>

<small>/v1/readers/commands</small>
```json
{
  "readerControllerId": "5eba9d3c87bc126cbf5b9dc4"
}
```

<small>/v1/readers/commands</small>
```json
{
  "readerControllerId": "5eba9d3c87bc126cbf5b9dc4",
  "data" : {
    "applicationId": "123465489798",
    "challenge": "0x90af000010c027047c809293e51c6a303514aa66c800"
  }
}
```

<small>/v1/applications/commands</small>
```json
{
  "applicationId": "123465489798",
  "data" : {
    "readerControllerId": "5eba9d3c87bc126cbf5b9dc4",
    "apdu": "0x901a0000010000"
  }
}
```




### Build and run
```bash
./gradlew clean build
java -jar build/libs/picc-reader-broker.jar
```

### Generate test metrics
```bash
./gradlew clean pitest
```

### Sonar
```bash
./gradlew clean build sonarqube -Dsonar.host.url=http://localhost:9000
```

### Local deployment
```bash

# Set Docker environment
eval $(minikube docker-env)

# Create config map
kubectl create configmap picc-reader-broker-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml

# Build
./gradlew clean build

# Docker build
docker build -t local/picc-reader-broker:$(git rev-parse --short HEAD) .

# Helm install
helm upgrade --install --force picc-reader-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/picc-reader-broker:$(git rev-parse --short HEAD)

# All together
./gradlew clean build && \
docker build -t local/picc-reader-broker:$(git rev-parse --short HEAD) . && \
helm upgrade --install --force picc-reader-broker ./kube \
    --namespace=default \
    --set spec.rules.host=cluster.local \
    --set image.application.name=local/picc-reader-broker:$(git rev-parse --short HEAD)


```