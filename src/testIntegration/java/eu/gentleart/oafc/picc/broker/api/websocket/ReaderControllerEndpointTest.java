package eu.gentleart.oafc.picc.broker.api.websocket;

import eu.gentleart.oafc.picc.broker.AbstractApiTest;
import eu.gentleart.oafc.picc.broker.api.ReaderControllerResourceTest;
import eu.gentleart.oafc.picc.broker.helpers.LocalFileReader;
import org.junit.jupiter.api.Test;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReaderControllerEndpointTest extends AbstractApiTest {

    @Test
    void given_a_valid_register_request_should_register_the_reader_controller_at_broker() throws IOException, DeploymentException, InterruptedException, TimeoutException {

        String readerControllerId = ReaderControllerResourceTest.createReaderController();

        String connectionMessage = LocalFileReader.readJsonFile("json/reader-controller-connect.json", LocalFileReader.changeField("readerId", readerControllerId));
        String authChallengeRequest = LocalFileReader.readJsonFile("json/commands-auth-challenge.json", LocalFileReader.changeField("data.readerId", readerControllerId));
        String authChallengeExpectedResponse = LocalFileReader.readJsonFile("json/commands-auth-challenge-response.json", LocalFileReader.changeField("readerId", readerControllerId));

        ApplicationAndReaderCommunication.start(getLocalServerPort())
                .readerConnectsToBroker()
                .applicationConnectsToBroker()
                .readerSendsMessageToBroker(connectionMessage)
                .applicationSendsMessageToBroker(authChallengeRequest)
                .readerReceivesMessageContaining("0x1a")
                .readerSendsMessageToBroker(authChallengeExpectedResponse)
                .applicationReceivesMessageContaining("applicationId", "challenge")
                .finish();
    }

    @Test
    void given_multiple_connections_from_the_same_application_should_use_the_most_recent_one() throws IOException, DeploymentException, TimeoutException, InterruptedException {
        String readerControllerId = ReaderControllerResourceTest.createReaderController();

        String connectionMessage = LocalFileReader.readJsonFile("json/reader-controller-connect.json", LocalFileReader.changeField("readerId", readerControllerId));
        String authChallengeRequest = LocalFileReader.readJsonFile("json/commands-auth-challenge.json", LocalFileReader.changeField("data.readerId", readerControllerId));
        String authChallengeExpectedResponse = LocalFileReader.readJsonFile("json/commands-auth-challenge-response.json", LocalFileReader.changeField("readerId", readerControllerId));

        ApplicationAndReaderCommunication.start(getLocalServerPort())
                .readerConnectsToBroker()
                .applicationConnectsToBroker()
                .applicationConnectsToBroker() // double connection
                .readerSendsMessageToBroker(connectionMessage)
                .applicationSendsMessageToBroker(authChallengeRequest)
                .readerReceivesMessageContaining("0x1a")
                .readerSendsMessageToBroker(authChallengeExpectedResponse)
                .applicationReceivesMessageContaining("applicationId", "challenge")
                .finish();
    }

    @Test
    void given_multiple_connections_from_the_same_reader_should_use_the_most_recent_one() throws IOException, DeploymentException, TimeoutException, InterruptedException {
        String readerControllerId = ReaderControllerResourceTest.createReaderController();

        String connectionMessage = LocalFileReader.readJsonFile("json/reader-controller-connect.json", LocalFileReader.changeField("readerId", readerControllerId));
        String authChallengeRequest = LocalFileReader.readJsonFile("json/commands-auth-challenge.json", LocalFileReader.changeField("data.readerId", readerControllerId));
        String authChallengeExpectedResponse = LocalFileReader.readJsonFile("json/commands-auth-challenge-response.json", LocalFileReader.changeField("readerId", readerControllerId));

        ApplicationAndReaderCommunication.start(getLocalServerPort())
                .readerConnectsToBroker()
                .readerConnectsToBroker() // double connection
                .applicationConnectsToBroker()
                .readerSendsMessageToBroker(connectionMessage)
                .applicationSendsMessageToBroker(authChallengeRequest)
                .readerReceivesMessageContaining("0x1a")
                .readerSendsMessageToBroker(authChallengeExpectedResponse)
                .applicationReceivesMessageContaining("applicationId", "challenge")
                .finish();
    }

}
