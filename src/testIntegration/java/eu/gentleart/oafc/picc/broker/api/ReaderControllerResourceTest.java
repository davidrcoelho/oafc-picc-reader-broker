package eu.gentleart.oafc.picc.broker.api;

import eu.gentleart.oafc.picc.broker.AbstractApiTest;
import eu.gentleart.oafc.picc.broker.helpers.OpenApiParser;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.hasKey;

public class ReaderControllerResourceTest extends AbstractApiTest  {

    @Test
    public void given_a_post_request_should_generate_a_new_token() {
        createReaderController();
    }

    public static String createReaderController() {
        String controllerRequest = OpenApiParser.exampleOf("ReaderControllerRequest");

        return
            given()
                    .contentType(ContentType.JSON)
                    .body(controllerRequest)
                    .post("/v1/controllers")
            .then()
                    .log().all()
                    .statusCode(200)
                    .body("$", hasKey("id"))
                    .body("$", hasKey("name"))
                    .body("$", hasKey("tags"))

                    .extract().path("id").toString();
    }

}
