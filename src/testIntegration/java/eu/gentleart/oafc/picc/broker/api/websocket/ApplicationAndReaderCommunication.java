package eu.gentleart.oafc.picc.broker.api.websocket;

import eu.gentleart.oafc.picc.broker.helpers.SyncWebSocketClient;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationAndReaderCommunication {

    private int wsBrokerPort;

    private SyncWebSocketClient readerConnection;
    private SyncWebSocketClient applicationConnection;


    public static ApplicationAndReaderCommunication start(int wsBrokerPort) {
        ApplicationAndReaderCommunication communication = new ApplicationAndReaderCommunication();
        communication.wsBrokerPort = wsBrokerPort;
        return communication;
    }

    public ApplicationAndReaderCommunication readerConnectsToBroker() throws IOException, DeploymentException {
        String url = String.format("ws://localhost:%s/v1/readers/commands", wsBrokerPort);
        readerConnection = new SyncWebSocketClient("[READER]", url, 2000);
        return this;
    }

    public ApplicationAndReaderCommunication applicationConnectsToBroker() throws IOException, DeploymentException {
        String url = String.format("ws://localhost:%s/v1/applications/commands", wsBrokerPort);
        applicationConnection = new SyncWebSocketClient("[APPLICATION]", url, 2000);
        return this;
    }

    public ApplicationAndReaderCommunication readerSendsMessageToBroker(String payload) {
        readerConnection.send(payload);
        return this;
    }

    public ApplicationAndReaderCommunication applicationSendsMessageToBroker(String payload) throws TimeoutException, InterruptedException {
        applicationConnection.send(payload);
        return this;
    }

    public ApplicationAndReaderCommunication readerReceivesMessageContaining(String ... values) throws TimeoutException, InterruptedException {
        assertThat(readerConnection.nextMessage()).contains(values);
        return this;
    }

    public ApplicationAndReaderCommunication applicationReceivesMessageContaining(String ... values) throws TimeoutException, InterruptedException {
        assertThat(applicationConnection.nextMessage()).contains(values);
        return this;
    }

    public void finish() throws IOException {
        readerConnection.close();
        applicationConnection.close();
    }
}
