package eu.gentleart.oafc.picc.broker;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.hasKey;

public class ActuatorApiTest extends AbstractApiTest {

    @Test
    public void should_have_info_endpoint() {
        given()
                .get("actuator/info")
        .then()
                .log().all()
                .statusCode(200)
                .body("app", hasKey("name"))
                .body("git", hasKey("branch"))
                .body("git.commit", hasKey("id"))
                .body("git.commit", hasKey("time"));
    }

}
