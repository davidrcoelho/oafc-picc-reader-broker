package eu.gentleart.oafc.picc.broker;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.GenericContainer;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {AbstractApiTest.PropertiesInitializer.class})
public abstract class AbstractApiTest {

        public static GenericContainer rabbitmq = new GenericContainer<>("rabbitmq:3.7-alpine")
                .withExposedPorts(5672);
        static {
            rabbitmq.start();
        }

        @LocalServerPort
        private int port;

        protected int getLocalServerPort() {
            return port;
        }

        @BeforeEach
        public void beforeEach() {
            RestAssured.port = port;
        }

        public static RequestSpecification given() {
            return RestAssured.given()
                    .log().all()
                    .filter(new OpenApiValidationFilter("apidoc.yaml"));
        }

    public static class PropertiesInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(configurableApplicationContext,
                    "spring.rabbitmq.host=localhost",
                    "spring.rabbitmq.port=" + rabbitmq.getMappedPort(5672)
            );
        }
    }

}
