package eu.gentleart.oafc.picc.broker.api.rest;

import eu.gentleart.oafc.picc.broker.controller.ReaderController;
import eu.gentleart.oafc.picc.broker.controller.Tag;
import lombok.Getter;

import java.util.List;

@Getter
public class ReaderControllerRequest {

    private String name;
    private List<Tag> tags;

    public ReaderController getData() {
        ReaderController readerController = new ReaderController();
        readerController.setName(name);
        readerController.setTags(tags);
        return readerController;
    }
}
