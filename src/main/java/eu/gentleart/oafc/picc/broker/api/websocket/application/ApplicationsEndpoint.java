package eu.gentleart.oafc.picc.broker.api.websocket.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Service("/v1/applications/commands")
public class ApplicationsEndpoint extends TextWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationsEndpoint.class);

    @Autowired
    private ApplicationMessageProcessor applicationMessageProcessor;

    @Override
    protected void handleTextMessage(WebSocketSession wsSession, TextMessage message) throws Exception {
        logger.info("Message received from application. {}", message.getPayload());
        applicationMessageProcessor.handleApplicationMessage(message.getPayload(), wsSession);
    }

}
