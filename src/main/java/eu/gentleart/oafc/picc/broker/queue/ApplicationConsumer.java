package eu.gentleart.oafc.picc.broker.queue;

import eu.gentleart.oafc.picc.broker.RabbitmqConfig;
import eu.gentleart.oafc.picc.broker.api.websocket.application.ApplicationSessionMultiplexer;
import eu.gentleart.oafc.picc.broker.api.websocket.reader.ReaderToApplicationMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ApplicationConsumer {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationConsumer.class);

    @Autowired
    private ApplicationSessionMultiplexer multiplexer;

    @RabbitListener(queues = RabbitmqConfig.READER_TO_APPLICATION_QUEUE)
    public void receiveMessage(ReaderToApplicationMessage message) throws IOException {

        logger.info("Received messaged: {}.", message);
        if (!multiplexer.hasChannelToTarget(message.getTargetId())) {
            logger.info("Rejecting from queue: {}.", message);
            throw new RejectMessageException();
        }
        multiplexer.sendMessage(message);
    }

}
