package eu.gentleart.oafc.picc.broker.api.websocket;

import eu.gentleart.oafc.picc.broker.api.websocket.application.ApplicationSessionMultiplexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractMultiplexer {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationSessionMultiplexer.class);

    protected final Map<String, WSSessionWrapper> sessions = new HashMap<>();

    public abstract boolean hasChannelToTarget(String applicationId);
    public abstract void sendMessage(Message message) throws IOException;

    public void addSession(String sessionKey, WSSessionWrapper sessionWrapper) throws IOException {
        logger.debug("Adding session. sessionKey: {}, sessions: {}, session: {}", sessionKey, sessions, sessionWrapper);

        if (sessions.containsKey(sessionKey)) {
            replaceAndCloseOldSession(sessionKey, sessionWrapper);

        } else {
            sessions.put(sessionKey, sessionWrapper);
        }

    }

    private void replaceAndCloseOldSession(String sessionKey, WSSessionWrapper sessionWrapper) throws IOException {
        WSSessionWrapper oldSession = sessions.get(sessionKey);
        if (!oldSession.equals(sessionWrapper)) {
            logger.info("Replacing old websocket session. sessionKey: {}, session: {}", sessionKey, oldSession);
            sessions.put(sessionKey, sessionWrapper);
            oldSession.close();
        } else {
            logger.info("Same session. Nothing to do.");
        }
    }

}
