package eu.gentleart.oafc.picc.broker;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitmqConfig {

    public static final String APPLICATION_TO_READER_QUEUE = "application-to-reader-queue";
    public static final String READER_TO_APPLICATION_QUEUE = "reader-to-application-queue";

    @Bean(name = "applicationToReaderQueue")
    public Queue commandQueue() {
        return QueueBuilder
                .nonDurable(APPLICATION_TO_READER_QUEUE)
                .withArgument("x-message-ttl", 2000)
                .build();
    }

    @Bean(name = "readerToApplicationQueue")
    public Queue applicationQueue() {
        return QueueBuilder
                .nonDurable(READER_TO_APPLICATION_QUEUE)
                .withArgument("x-message-ttl", 2000)
                .build();
    }

}
