package eu.gentleart.oafc.picc.broker.api.websocket.reader;

import eu.gentleart.oafc.picc.broker.api.websocket.Message;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashMap;

@Getter @ToString
public class ReaderToApplicationMessage implements Serializable, Message {

    @NotBlank
    private String readerId;

    private HashMap<String, Object> data;

    public boolean hasData() {
        return data != null && !data.isEmpty();
    }

    @Override
    public String getSourceId() {
        return readerId;
    }

    public String getTargetId() {
        return (String) data.get("applicationId");
    }
}
