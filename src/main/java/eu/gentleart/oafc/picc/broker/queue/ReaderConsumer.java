package eu.gentleart.oafc.picc.broker.queue;

import eu.gentleart.oafc.picc.broker.RabbitmqConfig;
import eu.gentleart.oafc.picc.broker.api.websocket.application.ApplicationToReaderMessage;
import eu.gentleart.oafc.picc.broker.api.websocket.reader.ReaderSessionMultiplexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ReaderConsumer {

    private static final Logger logger = LoggerFactory.getLogger(ReaderConsumer.class);

    @Autowired
    private ReaderSessionMultiplexer container;

    @RabbitListener(queues = RabbitmqConfig.APPLICATION_TO_READER_QUEUE)
    public void receiveMessage(ApplicationToReaderMessage message) throws IOException {

        logger.info("Received command: {}.", message);
        if (!container.hasChannelToTarget(message.getTargetId())) {
            logger.info("Rejecting from queue: {}.", message);
            throw new RejectMessageException();
        }
        container.sendMessage(message);
    }

}
