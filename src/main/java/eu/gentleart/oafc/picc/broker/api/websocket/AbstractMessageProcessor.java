package eu.gentleart.oafc.picc.broker.api.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

public class AbstractMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(AbstractMessageProcessor.class);

    private final AbstractMultiplexer sourceMultiplexer;
    private final AbstractMultiplexer targetMultiplexer;
    private final RabbitTemplate rabbitTemplate;

    public AbstractMessageProcessor(AbstractMultiplexer sourceMultiplexer,
                                    AbstractMultiplexer targetMultiplexer,
                                    RabbitTemplate rabbitTemplate) {
        this.sourceMultiplexer = sourceMultiplexer;
        this.targetMultiplexer = targetMultiplexer;
        this.rabbitTemplate = rabbitTemplate;
    }

    public void handleMessage(Message message, WebSocketSession wsSession, String targetQueueName) throws IOException {

        sourceMultiplexer.addSession(message.getSourceId(), new WSSessionWrapper(wsSession));

        if (message.hasData()) {
            logger.info("Message has data to send to the target. {}", message);
            if (targetMultiplexer.hasChannelToTarget(message.getTargetId())) {
                logger.info("Local connection to the target found. {}", message);
                targetMultiplexer.sendMessage(message);
            } else {
                logger.info("No local connection to the target found. Sending to the queue. {}", message);
                rabbitTemplate.convertAndSend(targetQueueName, message);
            }
        }
    }

}
