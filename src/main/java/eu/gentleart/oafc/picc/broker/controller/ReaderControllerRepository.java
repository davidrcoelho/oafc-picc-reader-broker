package eu.gentleart.oafc.picc.broker.controller;


@org.springframework.stereotype.Controller
public class ReaderControllerRepository {

    private ReaderControllerCrudRepository crudRepository;

    public ReaderControllerRepository(ReaderControllerCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    public ReaderController create(ReaderController readerController) {
        return crudRepository.save(readerController);
    }

    public boolean exists(String id) {
        return crudRepository.existsById(id);
    }

}
