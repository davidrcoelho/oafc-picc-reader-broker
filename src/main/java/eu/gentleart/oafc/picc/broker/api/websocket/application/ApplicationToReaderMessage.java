package eu.gentleart.oafc.picc.broker.api.websocket.application;

import eu.gentleart.oafc.picc.broker.api.websocket.Message;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashMap;

@Getter @ToString
public class ApplicationToReaderMessage implements Serializable, Message {

    @NotBlank
    private String applicationId;

    private HashMap<String, Object> data;

    public boolean hasData() {
        return data != null && !data.isEmpty();
    }

    @Override
    public String getSourceId() {
        return applicationId;
    }

    public String getTargetId() {
        return (String) data.get("readerId");
    }

}