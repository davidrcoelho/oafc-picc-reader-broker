package eu.gentleart.oafc.picc.broker.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import java.util.Date;
import java.util.List;

@Getter
public class ReaderController {

    @Id
    private String id;

    @CreatedDate
    private Date created;

    @LastModifiedDate
    private Date lastModified;

    @Version
    @JsonIgnore
    private long version;

    @Setter
    private String name;

    @Setter
    private List<Tag> tags;

}
