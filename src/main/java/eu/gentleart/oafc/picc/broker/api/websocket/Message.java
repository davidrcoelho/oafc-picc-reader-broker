package eu.gentleart.oafc.picc.broker.api.websocket;

public interface Message {

    boolean hasData();

    String getSourceId();

    String getTargetId();

}
