package eu.gentleart.oafc.picc.broker.api.rest;

import eu.gentleart.oafc.picc.broker.controller.ReaderController;
import eu.gentleart.oafc.picc.broker.controller.ReaderControllerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class ReaderControllersResource {

    @Autowired
    private ReaderControllerRepository repository;

    @PostMapping("/v1/controllers")
    public ResponseEntity<ReaderController> createReaderController(
            @RequestBody @Valid ReaderControllerRequest request) {

        ReaderController readerController = repository.create(request.getData());
        return new ResponseEntity(readerController, HttpStatus.OK);
    }

}
