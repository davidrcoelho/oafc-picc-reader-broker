package eu.gentleart.oafc.picc.broker.controller;

import lombok.Getter;

@Getter
public class Tag {

    private String key;
    private String value;

}
