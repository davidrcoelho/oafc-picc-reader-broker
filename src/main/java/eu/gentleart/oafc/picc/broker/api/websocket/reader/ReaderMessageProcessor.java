package eu.gentleart.oafc.picc.broker.api.websocket.reader;

import eu.gentleart.oafc.picc.broker.JsonParser;
import eu.gentleart.oafc.picc.broker.RabbitmqConfig;
import eu.gentleart.oafc.picc.broker.api.websocket.AbstractMessageProcessor;
import eu.gentleart.oafc.picc.broker.api.websocket.application.ApplicationSessionMultiplexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Component
public class ReaderMessageProcessor extends AbstractMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ReaderMessageProcessor.class);

    @Autowired
    public ReaderMessageProcessor(ReaderSessionMultiplexer readerSessionMultiplexer,
                                  ApplicationSessionMultiplexer applicationSessionMultiplexer,
                                  RabbitTemplate rabbitTemplate) {
        super(readerSessionMultiplexer, applicationSessionMultiplexer, rabbitTemplate);
    }

    public void handleReaderControllerMessage(String payload, WebSocketSession wsSession) throws IOException {
        logger.info("Handling message from reader. {}", payload);
        ReaderToApplicationMessage message = JsonParser.parse(payload, ReaderToApplicationMessage.class);
        handleMessage(message, wsSession, RabbitmqConfig.READER_TO_APPLICATION_QUEUE);
    }

}
