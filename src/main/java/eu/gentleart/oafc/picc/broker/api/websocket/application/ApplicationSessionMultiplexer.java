package eu.gentleart.oafc.picc.broker.api.websocket.application;

import eu.gentleart.oafc.picc.broker.api.websocket.AbstractMultiplexer;
import eu.gentleart.oafc.picc.broker.api.websocket.Message;
import eu.gentleart.oafc.picc.broker.api.websocket.WSSessionWrapper;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

@Component
@Scope(SCOPE_SINGLETON)
public class ApplicationSessionMultiplexer extends AbstractMultiplexer {

    public boolean hasChannelToTarget(String applicationId) {
        return sessions.containsKey(applicationId);
    }

    public void sendMessage(Message message) throws IOException {
        WSSessionWrapper session = sessions.get(message.getTargetId());
        session.send(message);
    }
}
