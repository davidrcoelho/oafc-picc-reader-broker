package eu.gentleart.oafc.picc.broker.queue;

public class RejectMessageException extends RuntimeException {
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
