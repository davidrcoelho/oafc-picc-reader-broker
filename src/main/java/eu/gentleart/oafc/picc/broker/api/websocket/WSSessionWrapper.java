package eu.gentleart.oafc.picc.broker.api.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.ToString;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Objects;

@ToString @Getter
public class WSSessionWrapper {

    private WebSocketSession session;

    public WSSessionWrapper(WebSocketSession session) {
        this.session = session;
    }

    public void send(Object message) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String payload = mapper.writeValueAsString(message);
        session.sendMessage(new TextMessage(payload));
    }

    public void close() throws IOException {
        session.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WSSessionWrapper that = (WSSessionWrapper) o;
        return Objects.equals(session, that.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(session);
    }
}
