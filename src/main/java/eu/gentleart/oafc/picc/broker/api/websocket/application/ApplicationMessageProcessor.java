package eu.gentleart.oafc.picc.broker.api.websocket.application;

import eu.gentleart.oafc.picc.broker.JsonParser;
import eu.gentleart.oafc.picc.broker.RabbitmqConfig;
import eu.gentleart.oafc.picc.broker.api.websocket.AbstractMessageProcessor;
import eu.gentleart.oafc.picc.broker.api.websocket.reader.ReaderSessionMultiplexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

@Component
public class ApplicationMessageProcessor extends AbstractMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationMessageProcessor.class);

    @Autowired
    public ApplicationMessageProcessor(ApplicationSessionMultiplexer applicationSessionMultiplexer,
                                       ReaderSessionMultiplexer readerSessionMultiplexer,
                                       RabbitTemplate rabbitTemplate) {
        super(applicationSessionMultiplexer, readerSessionMultiplexer, rabbitTemplate);
    }

    public void handleApplicationMessage(String payload, WebSocketSession wsSession) throws IOException {
        logger.info("Handling message from application. {}", payload);
        ApplicationToReaderMessage message = JsonParser.parse(payload, ApplicationToReaderMessage.class);
        handleMessage(message, wsSession, RabbitmqConfig.APPLICATION_TO_READER_QUEUE);
    }

}
