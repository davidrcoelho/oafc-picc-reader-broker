package eu.gentleart.oafc.picc.broker.controller;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReaderControllerCrudRepository extends MongoRepository<ReaderController, String> {
}
