package eu.gentleart.oafc.picc.broker.api.websocket.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Service("/v1/readers/commands")
public class ReadersEndpoint extends TextWebSocketHandler {

    private static final Logger logger = LoggerFactory.getLogger(ReadersEndpoint.class);

    @Autowired
    private ReaderMessageProcessor sessionController;

    @Override
    protected void handleTextMessage(WebSocketSession wsSession, TextMessage message) throws Exception {
        logger.info("Message received from reader. {}", message.getPayload());
        sessionController.handleReaderControllerMessage(message.getPayload(), wsSession);
    }

}
