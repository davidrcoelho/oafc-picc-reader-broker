package eu.gentleart.oafc.picc.broker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.util.Map;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

    private final Map<String, WebSocketHandler> handlers;

    @Autowired
    public WebSocketConfig(Map<String, WebSocketHandler> handlers) {
        this.handlers = handlers;
    }

    private void addWebsocketHandlers(WebSocketHandlerRegistry registry) {
        logger.info("Adding websocket handlers.");
        for (Map.Entry<String, WebSocketHandler> websocket : handlers.entrySet()) {
            logger.info("Adding path: {} to websocket: {}", websocket.getKey(), websocket.getValue());
            registry.addHandler(websocket.getValue(), websocket.getKey());
        }
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        addWebsocketHandlers(registry);
    }
}