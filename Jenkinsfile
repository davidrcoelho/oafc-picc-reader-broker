#!/usr/bin/env groovy

pipeline {

    agent { node {  label 'Jenkins' } }
    
    stages {

        stage('Unit+Integration') {
            steps {
                sh './gradlew clean build'
            }
        }

        stage('Sonar metrics') {
            steps {
                sh './gradlew clean build sonarqube '
            }
        }

        stage('Deploy Docker image') {
            steps {
                sh 'docker build -t local/picc-reader-broker:${BUILD_NUMBER}-$(git rev-parse --short HEAD) .'
                sh 'docker push local/picc-reader-broker:${BUILD_NUMBER}-$(git rev-parse --short HEAD) '
            }
        }

        stage('Deploy helm chart') {
            steps {
                sh "kubectl create configmap picc-reader-broker-apidoc --from-file=./src/testIntegration/resources/apidoc.yaml -o yaml --dry-run | kubectl apply -f -"
                sh "helm upgrade --install --force picc-reader-broker ./kube \
                    --namespace=default \
                    --set spec.rules.host=cluster.local \
                    --set image.application.name=local/picc-reader-broker:${BUILD_NUMBER}-\$(git rev-parse --short HEAD)"
            }
        }

    }
}
