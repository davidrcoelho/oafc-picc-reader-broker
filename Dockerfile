FROM openjdk:8-alpine

RUN mkdir /apps

COPY ./build/libs/picc-reader-broker.jar /apps/picc-reader-broker.jar

HEALTHCHECK --interval=10s --start-period=2m CMD curl -f http://localhost:8080/actuator/health | exit 1

CMD java -jar /apps/picc-reader-broker.jar

EXPOSE 8080
